import {assert, expect} from 'chai';
import {createGrid, getNeighbors, populateGrid, revealCell} from '../Grid';
import Cell, {CellState, CellType} from '../Cell';

describe('Grid', () => {
  describe('createGRid', () => {
    it('should return an array', () => {
      const grid = createGrid(0, 0);

      assert.isArray(grid);
    });

    it('will not accept a total length lower or equal to zero', () => {
      const width = 10;
      const height = 7;
      const grid = createGrid(width, height);

      validateGrid(grid, width, height);
    });

    it('should return an array with a proper size', () => {
      const width = 10;
      const height = 7;
      const grid = createGrid(width, height);

      validateGrid(grid, width, height);
    });

    it('should return an array filled with Cells', () => {
      const grid = createGrid(10, 10);

      assert.isArray(grid);

      grid.forEach(cell => {
        expect(cell).to.not.be.undefined;
        expect(cell).to.be.instanceOf(Cell);
        expect(cell.state).to.be.equal(CellState.HIDDEN);
        expect(cell.type).to.be.equal(CellType.SAFE);
      });
    });
  });

  describe('revealCell', () => {
    it('should return a new grid with same amount of bomb and cell', () => {
      const width = 2;
      const height = 2;
      const bombQuantity = 2;
      const grid = populateGrid(createGrid(width, height), bombQuantity);

      const updatedGrid = revealCell(grid, 2, 0);
      expect(updatedGrid).to.not.be.undefined;
      expect(updatedGrid).to.have.lengthOf(width * height);

      validateBombQuantity(grid, bombQuantity);
    });

    it('should propagate the reveal process as long as the revealed cell does not have a bomb for neighbor', () => {
      const bomb = new Cell(CellState.HIDDEN, CellType.BOMB);
      const safe = new Cell(CellState.HIDDEN, CellType.SAFE);
      const grid = [
        bomb, safe, safe, safe, safe,
        bomb, safe,  safe, safe, safe,
        bomb, bomb,  safe, safe, safe,
      ];
      const originalGridLength = grid.length;

      expect(grid).to.have.lengthOf(originalGridLength);
      validateBombQuantity(grid, 4);
      validateRevealedQuantity(grid, 0);

      const updatedGrid = revealCell(grid, 5, originalGridLength-1);
      expect(updatedGrid).to.not.be.undefined;
      expect(updatedGrid).to.have.lengthOf(originalGridLength);

      validateBombQuantity(updatedGrid, 4);
      validateRevealedQuantity(grid, 11);
    });

    it('should return a new grid with the proper cell revealed if at least one neighbor is a bomb', () => {
      const bomb = new Cell(CellState.HIDDEN, CellType.BOMB);
      const safe = new Cell(CellState.HIDDEN, CellType.SAFE);
      const known = new Cell(CellState.VISIBLE, CellType.SAFE);
      const grid = [
        bomb, known, known,
        bomb, safe, known,
        bomb, bomb, known,
      ];
      const originalGridLength = grid.length;

      expect(grid).to.have.lengthOf(originalGridLength);
      validateBombQuantity(grid, 4);
      validateRevealedQuantity(grid, 4);

      const updatedGrid = revealCell(grid, 3, 4);
      expect(updatedGrid).to.not.be.undefined;
      expect(updatedGrid).to.have.lengthOf(originalGridLength);

      validateBombQuantity(updatedGrid, 4);
      validateRevealedQuantity(updatedGrid, 5);
    });
  });

  it('should return a new grid with the proper cell and its neighbor revealed if no bomb is found next to the target cell', () => {
    const safe = new Cell(CellState.HIDDEN, CellType.SAFE);
    const known = new Cell(CellState.VISIBLE, CellType.SAFE);
    const grid = [
      safe, known, known,
      safe, safe, known,
      safe, safe, known,
    ];
    const originalGridLength = grid.length;

    expect(grid).to.have.lengthOf(originalGridLength);
    validateBombQuantity(grid, 0);
    validateRevealedQuantity(grid, 4);

    const updatedGrid = revealCell(grid, 3, 4);
    expect(updatedGrid).to.not.be.undefined;
    expect(updatedGrid).to.have.lengthOf(originalGridLength);

    validateBombQuantity(updatedGrid, 0);
    validateRevealedQuantity(updatedGrid, originalGridLength);
  });

  describe('getNeighbors', () => {
    it('return a list of cell', () => {
      const grid = createGrid(2, 2);
      const neighbors = getNeighbors(grid.length, 2, 0);
      expect(neighbors).to.not.be.undefined;
      assert.isArray(neighbors);
    });

    it('return all neighbors of given cell index', () => {
      const safe = new Cell(CellState.HIDDEN, CellType.SAFE);
      const known = new Cell(CellState.VISIBLE, CellType.SAFE);
      const grid = [
        safe, known, known,
        safe, safe, known,
        safe, safe, known,
      ];

      // Get neighbors of the middle cell
      const neighbors = getNeighbors(grid.length, 3, 4);

      expect(neighbors).to.not.be.undefined;
      assert.isArray(neighbors);
      expect(neighbors).to.have.lengthOf(8);
    });

    it('return all neighbors of given cell index excluding out of bound indexes', () => {
      const safe = new Cell(CellState.HIDDEN, CellType.SAFE);
      const known = new Cell(CellState.VISIBLE, CellType.SAFE);
      const grid = [
        safe, known, known,
        safe, safe, known,
        safe, safe, known,
      ];

      // Get neighbors of the top left cell
      let neighbors = getNeighbors(grid.length, 3, 0);

      expect(neighbors).to.not.be.undefined;
      assert.isArray(neighbors);
      expect(neighbors).to.have.lengthOf(3);

      // Get neighbors of the middle right cell
      neighbors = getNeighbors(grid.length, 3, 5);

      expect(neighbors).to.not.be.undefined;
      assert.isArray(neighbors);
      expect(neighbors).to.have.lengthOf(5);
    });
  });

  describe('populateGrid', () => {
    it('should accept a grid and a bomb quantity', () => {
      const width = 1;
      const height = 5;
      const bombQuantity = 5;
      const grid = populateGrid(createGrid(width, height), bombQuantity);

      validateGrid(grid, width, height);
      validateBombQuantity(grid, bombQuantity);
    });
  });
});

const validateRevealedQuantity = (grid, reveadedQuantity) => {
  const visibleCells = grid.filter(cell => cell.state === CellState.VISIBLE);
  expect(visibleCells.length).to.be.equal(reveadedQuantity);
};
const validateBombQuantity = (grid, bombQuantity) => {
  const bombs = grid.filter(cell => cell.type === CellType.BOMB);
  expect(bombs.length).to.be.equal(bombQuantity);
};

const validateGrid = (grid, width, height) => {
  assert.isArray(grid);
  expect(grid).to.have.lengthOf(height * width);
  expect(grid[0]).to.not.be.undefined;
};
