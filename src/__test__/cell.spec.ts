import {assert, expect} from 'chai';
import Cell, {CellState, CellType} from '../Cell';

describe('Cell', () => {

  it('is created with default values', () => {
    const cell = new Cell();
    expect(cell).to.not.be.undefined;
    expect(cell).to.be.instanceOf(Cell);
    expect(cell.state).to.be.equal(CellState.HIDDEN);
    expect(cell.type).to.be.equal(CellType.SAFE);
  });

  it('is created with default values', () => {
    const type = CellType.BOMB;
    const state = CellState.VISIBLE;
    const cell = new Cell(state,type);

    expect(cell).to.not.be.undefined;
    expect(cell).to.be.instanceOf(Cell);
    expect(cell.state).to.be.equal(state);
    expect(cell.type).to.be.equal(type);
  });

});
