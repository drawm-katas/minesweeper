import {filter, times, fill} from 'lodash';
import Cell, {CellState, CellType} from './Cell';

export const createGrid = (width: number, height: number): Cell[] => {
  const grid = fill(Array(height * width), null);
  return grid.map(() => new Cell());
};


export const populateGrid = (grid: Cell[], bombQuantity) => {
  const indexList = times(grid.length, time => time);
  const extract = extractRandomCellIndex(indexList);

  times(bombQuantity, extract)
    .forEach(index => {
      grid[index].type = CellType.BOMB;
    });


  return grid; // break mutation
};

const extractRandomCellIndex = (indexList) => () => indexList.splice(Math.floor(Math.random() * indexList.length), 1);


export const revealCell = (grid: Cell[], gridWidth: number, cellIndex: number): Cell[] => {
  grid[cellIndex].state = CellState.VISIBLE;

  const neighbors = getNeighbors(grid.length, gridWidth, cellIndex);
  const neighborsBombCount = neighbors.filter(index => grid[index].type === CellType.BOMB).length;

  grid[cellIndex].neighborBombCount = neighborsBombCount;

  if (neighborsBombCount === 0) {
    neighbors
      .filter(index => grid[index].state === CellState.HIDDEN)
      .forEach(index => revealCell(grid, gridWidth, index))
  }

  return grid;
};

const isPositionIngrid = (gridWidth, gridLength) => ({x, y}) =>
x >= 0 &&
y >= 0 &&
x < gridWidth &&
y < Math.floor(gridLength / gridWidth);

const offsetPosition = (x, y) => (position) => {
  return {
    ...position,
    x : position.x + x,
    y : position.y + y,
  };
};

const positionToIndex = (gridWidth) => ({x, y}) => x + y * gridWidth;

const getNeighboringBomb = (grid: Cell[], gridWidth: number, cellIndex: number): number[] => (
  getNeighbors(grid.length, gridWidth, cellIndex)
    .filter(index => grid[index].type === CellType.BOMB)
);
export const getNeighbors = (gridLength: number, gridWidth: number, cellIndex: number): number[] => {
  const x = Math.floor(cellIndex % gridWidth);
  const y = Math.floor(cellIndex / gridWidth);

  const positions = [
    {y : -1, x : -1}, {y : -1, x : 0}, {y : -1, x : 1},
    {y : 0, x : -1}, {y : 0, x : 1},
    {y : 1, x : -1}, {y : 1, x : 0}, {y : 1, x : 1},
  ];

  return positions
    .map(offsetPosition(x, y))
    .filter(isPositionIngrid(gridWidth, gridLength))
    .map(positionToIndex(gridWidth))

};

export const detectAllBombs = (grid: Cell[], gridWidth: number): Cell[] => {
  times(grid.length - 1, index => grid[index].neighborBombCount = getNeighboringBomb(grid, gridWidth, index).length);

  return grid;
};
