import Cell, {CellState, CellType} from "./Cell";
import {Text, Graphics, Container, TextStyle} from 'pixi.js';

const bombCountTextStyle = new TextStyle({
  fontFamily : 'Arial',
  fontSize : 16,
  fontStyle : 'italic',
  fontWeight : 'bold',
  fill : ['#EEEEEE', '#00ff99'], // gradient
  stroke : '#4a1850',
  strokeThickness : 2,
  dropShadow : true,
  dropShadowColor : '#000000',
  dropShadowBlur : 4,
  dropShadowAngle : Math.PI / 6,
  dropShadowDistance : 2,
  wordWrap : true,
  wordWrapWidth : 440
});

export const renderGrid = (grid: Cell[], gridWidth: number, cellSize: number, view: Graphics, textContainer: Container) => {
  textContainer.removeChildren();
  view.clear();

  view.lineStyle(1, 0);
  grid.forEach((cell, index) => {
    const x = Math.floor(index % gridWidth) * cellSize;
    const y = Math.floor(index / gridWidth) * cellSize;

    view.beginFill(0xEEEEEE);
    view.drawRect(x, y, cellSize, cellSize);

    if (cell.state === CellState.VISIBLE) {
      view.beginFill(0xEEEEEE);
      view.drawRect(x, y, cellSize, cellSize);

      if (cell.type == CellType.BOMB) {
        view.beginFill(0xFF0000);
        view.drawCircle(x + (cellSize >> 1), y + (cellSize >> 1), cellSize >> 2);

      } else if (cell.neighborBombCount > 0) {
        const bombCountText = new Text(cell.neighborBombCount.toString(), bombCountTextStyle);
        // bombCountText.width = bombCountText.height = cellSize;
        bombCountText.x = x + cellSize * .45;
        bombCountText.y = y + cellSize * .35;
        textContainer.addChild(bombCountText);
      }
    } else {
      view.beginFill(0xBBBBBB);
      view.drawRect(x, y, cellSize, cellSize);
    }
  });
};
