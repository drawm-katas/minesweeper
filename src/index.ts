import {interaction, Application, Text, Container, Graphics, TextStyle} from 'pixi.js';
import {createGrid, detectAllBombs, populateGrid, revealCell} from "./Grid";
import {renderGrid} from "./Renderer";
import {CellState, CellType} from "./Cell";

const app = new Application(500, 400, {backgroundColor : 0x1099bb});
document.body.appendChild(app.view);

const graphic = new Graphics();
app.stage.addChild(graphic);
const textContainer = new Container();
app.stage.addChild(textContainer);

const cellSize = 35;
const bombQuantity = 20;
const gridWidth = Math.floor(app.view.width / cellSize);
const gridHeight = Math.floor(app.view.height / cellSize);
const offsetX = app.view.width % cellSize;
const offsetY = app.view.height % cellSize;
let grid = null;

const gameOverTextStyle = new TextStyle({
  fontFamily : 'Arial',
  fontSize : 36,
  fontStyle : 'italic',
  fontWeight : 'bold',
  fill : ['#EEEEEE', '#00ff99'], // gradient
  stroke : '#4a1850',
  strokeThickness : 2,
  dropShadow : true,
  dropShadowColor : '#000000',
  dropShadowBlur : 4,
  dropShadowAngle : Math.PI / 6,
  dropShadowDistance : 2,
  wordWrap : true,
  wordWrapWidth : 440
});

const gameOverText = new Text("Game Over", gameOverTextStyle);
gameOverText.x = (app.view.width - gameOverText.width) >> 1;
gameOverText.y = (app.view.height - gameOverText.height) >> 1;
gameOverText.visible = false;
app.stage.addChild(gameOverText);

const winText = new Text("You Win", gameOverTextStyle);
winText.x = (app.view.width - winText.width) >> 1;
winText.y = (app.view.height - winText.height) >> 1;
winText.visible = false;
app.stage.addChild(winText);

graphic.x = offsetX >> 1;
graphic.y = offsetY >> 1;

const revealAllCells = () => {
  const endGrid = grid.map(cell => ({...cell, state : CellState.VISIBLE}));
  renderGrid(endGrid, gridWidth, cellSize, graphic, textContainer);
};


const resetGame = (event: interaction.InteractionEvent) => {

  // Clean up
  graphic.clear();
  textContainer.removeChildren();

  // Remove game state text
  gameOverText.visible = false;
  winText.visible = false;

  // Remove reset event
  event.target.interactive = false;
  event.target.off('pointerdown', resetGame);

  startGame();
};

const startGame = () => {
  // Reset grid
  grid = createGrid(gridWidth, gridHeight);
  grid = populateGrid(grid, bombQuantity);
  grid = detectAllBombs(grid, gridWidth);

  // Render initial grid
  renderGrid(grid, gridWidth, cellSize, graphic, textContainer);

  // Add click event
  graphic.interactive = true;
  graphic.on('pointerdown', onClick);
};

function gameOver() {
  textContainer.alpha = graphic.alpha = .7;

  gameOverText.visible = true;


  app.stage.interactive = true;
  app.stage.on('pointerdown', resetGame);

  revealAllCells();
}

const win = () => {
  textContainer.alpha = graphic.alpha = .7;

  winText.visible = true;

  app.stage.interactive = true;
  app.stage.on('pointerdown', resetGame);

  revealAllCells();
};

const onClick = (event: interaction.InteractionEvent) => {
  const position = event.data.getLocalPosition(event.target);
  const cellIndex = Math.floor(position.x / cellSize) + Math.floor(position.y / cellSize) * gridWidth;

  if (grid[cellIndex].state === CellState.VISIBLE) {
    return;
  }

  revealCell(grid, gridWidth, cellIndex);
  renderGrid(grid, gridWidth, cellSize, graphic, textContainer);

  if (grid[cellIndex].type === CellType.BOMB) {
    gameOver();

    event.target.interactive = false;
    event.target.off('pointerdown', onClick);

  } else if (grid.filter(cell => cell.state === CellState.HIDDEN).every(cell => cell.type === CellType.BOMB)) {
    win();

    event.target.interactive = false;
    event.target.off('pointerdown', onClick);
  }
};

startGame();
