import {Graphics} from 'pixi.js';

export enum CellType {
  BOMB,
  SAFE,
}
export enum CellState {
  HIDDEN,
  VISIBLE,
}

export default class Cell {
  public neighborBombCount:number = 0;

  constructor(
    public state: CellState = CellState.HIDDEN,
    public type: CellType = CellType.SAFE,
  ) {
  }
}